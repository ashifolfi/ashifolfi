- 👋 Hi! Ash here! I'm interested in programming applications and games.
- The languages I know how to code in are **Objective C, JS, Qt, C++, C, CSS, HTML, ZScript, and Lua**.
- I'm not looking to collaborate on just anything. If I want to help with your project I'll contact you.
- 📫 Aside from that you can find me at these places:
  - @RazzDaze on Twitter (I don't frequently check my DMs)
  - @ashi@meow.social on Mastodon
  - <insert untypable discord tag> (Highest chance of getting a response)
  - anticream39@yahoo.com

Other interesting facts about me inlude:
  - I prefer macOS for my main OS
  - KDE > Anything GNOME Based by a longshot
  - Huge Sonic fan
  - Heavy experience with SRB2 and the Doom engine
  
<!---
ashifolfi/ashifolfi is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
